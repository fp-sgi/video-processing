override SGI_ROOT_DIR ?= /Volumes/webdav/SGI

SCR_SOURCE_DIR      := ${SGI_ROOT_DIR}/Screencasts/Camtasia
SCR_MP4_TARGET_DIR  := ${SGI_ROOT_DIR}/Screencasts/MP4
SCR_META_TARGET_DIR := ${SGI_ROOT_DIR}/Screencasts/Metadata

CAM_SOURCE_DIR      := ${SGI_ROOT_DIR}/Videokamera/MTS
CAM_MP4_TARGET_DIR  := ${SGI_ROOT_DIR}/Videokamera/MP4
CAM_META_TARGET_DIR := ${SGI_ROOT_DIR}/Videokamera/Metadata

scr_sources      := $(shell find $(SCR_SOURCE_DIR) -type f -name '*.trec')
scr_mp4_targets  := $(patsubst $(SCR_SOURCE_DIR)/%.trec, $(SCR_MP4_TARGET_DIR)/%.mp4, $(scr_sources))
scr_meta_targets := $(patsubst $(SCR_SOURCE_DIR)/%.trec, $(SCR_META_TARGET_DIR)/%.json, $(scr_sources))

cam_sources      := $(shell find $(CAM_SOURCE_DIR) -type f -name '*.MTS')
cam_mp4_targets  := $(patsubst $(CAM_SOURCE_DIR)/%.MTS, $(CAM_MP4_TARGET_DIR)/%.mp4, $(cam_sources))
cam_meta_targets := $(patsubst $(CAM_SOURCE_DIR)/%.MTS, $(CAM_META_TARGET_DIR)/%.json, $(cam_sources))

.PHONY: all inventory conversion scr_to_meta cam_to_meta scr_to_mp4 cam_to_mp4
all: inventory conversion
inventory: scr_to_meta cam_to_meta
conversion: scr_to_mp4 cam_to_mp4
scr_to_meta: $(scr_meta_targets)
scr_to_mp4:  $(scr_mp4_targets)
cam_to_meta: $(cam_meta_targets)
cam_to_mp4:  $(cam_mp4_targets)

$(SCR_META_TARGET_DIR)/%.json: $(SCR_SOURCE_DIR)/%.trec
	@echo "inventorizing -> $(@)"
	@mkdir -p "$(@D)"
	@exiftool -json -x SourceFile -x Directory -x ExifToolVersion "$<" > "$(@)"

$(CAM_META_TARGET_DIR)/%.json: $(CAM_SOURCE_DIR)/%.MTS
	@echo "inventorizing -> $(@)"
	@mkdir -p "$(@D)"
	@exiftool -json -x SourceFile -x Directory -x ExifToolVersion "$<" > "$(@)"

$(SCR_MP4_TARGET_DIR)/%.mp4: $(SCR_SOURCE_DIR)/%.trec
	@echo "converting -> $(@)"
	@mkdir -p "$(@D)"
	@ffmpeg -v error -i "$<" -c:v libx264 -pix_fmt yuv420p -vf "scale=640:480" -crf 28 -tune animation -c:a libfaac "$(@)"

$(CAM_MP4_TARGET_DIR)/%.mp4: $(CAM_SOURCE_DIR)/%.MTS
	@echo "converting -> $(@)"
	@mkdir -p "$(@D)"
	@ffmpeg -v error -i "$<" -c:v libx264 -vf "scale=640:-1,unsharp" -crf 28 -c:a libfaac "$(@)"
